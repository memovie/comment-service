package id.phully.comment.controllers;

import id.phully.comment.models.Comment;
import id.phully.comment.models.CommentCustom;
import id.phully.comment.repositories.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/public/comment/")
public class PublicController {

    @Autowired
    CommentRepository commentRepository;

    @GetMapping("/{postId}")
    public Page<Comment> getAllPostComment(Pageable pageable,  @PathVariable("postId") String postId) {
        return commentRepository.findAllByPostId(pageable, postId);
    }
}
