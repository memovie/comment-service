package id.phully.comment.controllers;

import id.phully.comment.models.Comment;
import id.phully.comment.repositories.CommentRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/comment/")
public class AdminController {

    @Autowired
    CommentRepository commentRepository;

    @GetMapping("/{postId}")
    @PreAuthorize("hasAnyAuthority('Administrator')")
    public List<Comment> getAllComment() {
        return commentRepository.findAll();
    }

    @DeleteMapping("/{commentId}")
    @PreAuthorize("hasAnyAuthority('Administrator')")
    public void deleteComment(@PathVariable("postId") String commentId) {
        commentRepository.deleteById(commentId);
    }

}
