package id.phully.comment.controllers;

import id.phully.comment.models.Comment;
import id.phully.comment.models.CommentCustom;
import id.phully.comment.models.User;
import id.phully.comment.repositories.CommentRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user/comment")
public class UserController {

    @Autowired
    CommentRepository commentRepository;

    @GetMapping("/")
    @PreAuthorize("hasAnyAuthority('Administrator', 'User')")
    public List<CommentCustom> getAllComment(@RequestBody String userId, @RequestBody String postId) {
        return commentRepository.findAllCommentUserLikeDislike(userId, postId);
    }

    @PostMapping("/")
    @PreAuthorize("hasAnyAuthority('Administrator', 'User')")
    public Comment createComment(@RequestBody Comment comment) {
        return commentRepository.save(comment);
    }

    @DeleteMapping("/{commentId}")
    @PreAuthorize("hasAnyAuthority('Administrator', 'User')")
    public void deleteComment(@PathVariable("commentId") String commentId) {
        commentRepository.deleteById(commentId);
    }

}
