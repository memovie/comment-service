package id.phully.comment.repositories;

import id.phully.comment.models.Comment;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends MongoRepository<Comment, String>, CommentRepositoryCustom {
    @Query(sort = "{ _id : -1}")
    Page<Comment> findAllByPostId(Pageable pageable, String postId);

}
