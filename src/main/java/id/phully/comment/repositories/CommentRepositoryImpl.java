package id.phully.comment.repositories;

import id.phully.comment.models.CommentCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CommentRepositoryImpl implements CommentRepositoryCustom {

    private final MongoTemplate mongoTemplate;

    @Autowired
    public CommentRepositoryImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public List<CommentCustom> findAllCommentUserLikeDislike(String userId, String postId) {

        return mongoTemplate.aggregate(Aggregation.newAggregation(
                Aggregation.match(Criteria.where("postId").is(postId)),
                Aggregation.project()
                        .and("user").as("user")
                        .and("postId").as("postId")
                        .and("message").as("message")
                        .and("likes").size().as("countLike")
                        .and(ArrayOperators.IndexOfArray.arrayOf("likes").indexOf(userId)).as("likeStatus")
                        .and("dislike").size().as("countDislike")
                        .and(ArrayOperators.IndexOfArray.arrayOf("dislike").indexOf(userId)).as("dislikeStatus")
        ), CommentCustom.class, CommentCustom.class).getMappedResults();
    }


}
