package id.phully.comment.repositories;


import id.phully.comment.models.CommentCustom;

import java.util.List;

public interface CommentRepositoryCustom {
    List<CommentCustom> findAllCommentUserLikeDislike(String userId, String postId);
}
