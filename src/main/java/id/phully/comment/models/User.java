package id.phully.comment.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class User {

    private String avatar;
    private String fullName;
    private String uid;

    public User(String avatar, String fullName, String uid) {
        this.avatar = avatar;
        this.fullName = fullName;
        this.uid = uid;
    }
}
