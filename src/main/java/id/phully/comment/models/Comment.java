package id.phully.comment.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Document(collection = "movie_comments")
@Setter
@Getter
@NoArgsConstructor
@ToString(exclude = "_id")
public class Comment implements Serializable {
    @Id
    public String _id;

    @Field("user")
    private User user;

    private String postId;

    private String message;

    private List<String> likes = new ArrayList<>();

    private int countLike;

    private int likeStatus;

    public Comment(String _id, User user, String postId, String message, List<String> likes, int countLike, int likeStatus) {
        this._id = _id;
        this.user = user;
        this.postId = postId;
        this.message = message;
        this.likes = likes;
        this.countLike = countLike;
        this.likeStatus = likeStatus;
    }
}
