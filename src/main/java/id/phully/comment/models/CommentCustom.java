package id.phully.comment.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Document(collection = "movie_comments")
@Setter
@Getter
@NoArgsConstructor
@ToString(exclude = "_id")
public class CommentCustom implements Serializable {
    @Id
    public String _id;

    @Field("user")
    private User user;

    private String postId;

    private String message;

    private int countLike;

    private int likeStatus;

    private int countDislike;

    private int disLikeStatus;

    public CommentCustom(String _id, User user, String postId, String message, int countLike, int likeStatus, int countDislike, int disLikeStatus) {
        this._id = _id;
        this.user = user;
        this.postId = postId;
        this.message = message;
        this.countLike = countLike;
        this.likeStatus = likeStatus;
        this.countDislike = countDislike;
        this.disLikeStatus = disLikeStatus;
    }
}
